
//Functions Demo
//Spencer Sauberlich
#include <iostream>
#include <conio.h>

using namespace std;

//Function Prototype
void printName(int count = 3);
int add(int &a, int b);
bool divide(float num, float denom, float &answer);
void countDownFrom(int number);

int main()
{
	/*int x = 2;
	int y = 6;
	printName(add(x,y));

	cout << x << "\n";
	cout << y << "\n";
*/

	//float num1 = 0;
	//float num2 = 0;
	//float ans = 0;

	//cout << "Enter 2 numbers:";
	//cin >> num1;
	//cin >> num2;

	//if (divide(num1, num2, ans))
	//{
	//	cout << num1 << " divided by " << num2 << " is "
	//		<< ans << "\n";
	//}
	//else
	//{
	//	cout << "You cannot divide by zero!\n";
	//}

	char input = 'y';
	while (input == 'y')
	{
		countDownFrom(10);
		cout << "Again? (y/n):";
		cin >> input;
	}

	countDownFrom(10);

	_getch();
	return 0;
}

void countDownFrom(int number)
{
	cout << number << "\n";
	if (number == 0) return;
	countDownFrom(number - 1);
}

bool divide(float num, float denom, float &answer)
{
	if (denom == 0) return false;

	answer = num / denom;
	return true;
}

int add(int &a, int b)
{
	a++;
	b++;
	return a + b;
}


void printName(int count)
{
	for (int i = 0; i < count; i++)
	{
		cout << "Spencer\n";
	}
}
